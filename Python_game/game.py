# Name: Pooja Kaji

# Game name: the curse of the 7 society

# Game plot: you have been sent an invitation to join the 7 society, one of the most secretive societies on grounds. But you must first find the location of said meeting

# Setting: uva. Underground tunnel/caves/creepy stuff

# Game goal: pass a series of tests in order to prove your worthiness and reach the 7 society meeting
# There will be probably 2-3 puzzles / tests that the player must solve in order to move on in the game
# The objective is to see if you are worthy enough to join the 7 society at UVA. In order to prove your worthiness, you must complete all the puzzles/tests successfully.

# Ideas for Puzzles: (when changing puzzles, change the background)
# Puzzle 1 (A student’s worst nightmare): Given like certain number of hurdles made out of textbooks they have to jump over (vary in height). They must jump over 10 hurdles.
    # This signifies that the player can handle heavy workloads - passing the test allows them to move on to the second portion of the game (puzzle 2).
    # Have 3 lives(# of procrastinations). If they run out of lives, the game is over and they have to start from the beginning.
# Puzzle 2 (maze):
# Puzzle 3 (darts- heart-shaped):

#DISCLAIMER- In-order for the game to work, all images must be downloaded

import pygame
import gamebox
import random


camera = gamebox.Camera(800, 600)

# TITLE PAGE

game_started = 0
instructions = '''
~ There are three challenges for you to overcome ~

Use the arrow keys and space to move the player around
Press space to begin!
'''


def draw_title(keys):
   """
   Draws all components of the title screen
   :param keys:
   :return:
   """

   global game_started

   if keys:  # Start the game if any key at all pressed
       game_started = 1
   keys.clear()

   to_draw_title = []

   # Make the Game Title text
   title_box = gamebox.from_text(400, 150, 'The Secret 7 Society!', 80, 'white')
   to_draw_title.append(title_box)

   # Draw the instructions
   ypos = 250
   for line in instructions.split('\n'):
       to_draw_title.append(gamebox.from_text(400, ypos, line, 30, 'orangered3'))
       ypos += 30

   # decorate
   top_left_seven = gamebox.from_image(40, 170, 'seven society logo.png')
   to_draw_title.append(top_left_seven)
   top_right_seven = gamebox.from_image(730, 170, 'seven society logo.png')
   to_draw_title.append(top_right_seven)
   bottom_left_seven = gamebox.from_image(40, 650, 'seven society logo.png')
   to_draw_title.append(bottom_left_seven)
   bottom_right_seven = gamebox.from_image(730, 650, 'seven society logo.png')
   to_draw_title.append(bottom_right_seven)
   for box in to_draw_title:
       camera.draw(box)


### VARIABLES AND STUFF #####

# place 1
background1 = gamebox.from_image(400, 300, 'uvabackground.jpg')
background1.scale_by(0.59)

floor1 = gamebox.from_color(400, 600, 'red', 700, 140)

# place 2
background2 = gamebox.from_image(400, 300, 'uvabackground2.png')
background2.scale_by(0.6)

floor2 = gamebox.from_color(400, 600, 'red', 700, 330)

# place 3
background3 = gamebox.from_image(400, 300, 'uvabackground3.jpeg')

# ceiling
sky = gamebox.from_color(500, -50, 'black', 600, 100)

# letters
letter1 = gamebox.from_image(400, 500, 'closed_scroll.png')
letter1.scale_by(0.05)

open_letter = gamebox.from_image(400, 300, 'game_scroll.png')
open_letter.scale_by(0.8)

# sprite sheet
leftimages = gamebox.load_sprite_sheet('sprite_sheet_left.png', 1, 9)
rightimages = gamebox.load_sprite_sheet('sprite_sheet_right.png', 1, 9)
downimages = gamebox.load_sprite_sheet('sprite_sheet_down.png', 1, 9)
upimages = gamebox.load_sprite_sheet('sprite_sheet_up.png', 1, 9)

player1 = gamebox.from_image(200, 520, downimages[0])
player1.y = 520
player1_standing = True

# variables
tick_count = 0

mylives = 3

to_draw_part1 = []
letter_opened = False

to_draw_part1.append(floor1)
to_draw_part1.append(background1)
to_draw_part1.append(letter1)


def reset_player1():
   """
   places the walker on a random side of the screen at a random y position
   """
   if player1.left > camera.right:
       player1.right = camera.left
   elif player1.right < camera.left:
       player1.left = camera.right


def draw_part1(keys):
   global floor1
   global letter1
   global letter_opened
   global game_started
   global tick_count
   global player1_standing

   if player1_standing:
       player1.image = downimages[0]

   tick_count += 1

   to_draw_part1.append(player1)

   if pygame.K_RIGHT in keys:
       player1.x += 7
       player1.image = rightimages[tick_count // 2 % len(rightimages)]
   if pygame.K_LEFT in keys:
       player1.standing = False
       player1.x -= 7
       player1.image = leftimages[tick_count // 2 % len(leftimages)]
   if pygame.K_UP in keys:
       player1.y -= 7
       player1.image = upimages[tick_count // 2 % len(upimages)]
   if pygame.K_DOWN in keys:
       player1.y += 7
       player1.image = downimages[tick_count // 2 % len(downimages)]
   if player1.touches(letter1):
       to_draw_part1.append(open_letter)
       letter_opened = True
   if letter_opened and pygame.K_SPACE in keys:
       game_started = 1.5

   for thing in to_draw_part1:
       camera.draw(thing)

   if player1.left > camera.right or player1.right < camera.left:
       reset_player1()

   player1.move_to_stop_overlapping(floor1)
   player1.move_speed()


##################
##### PART 2 #####
##################

def draw_intro_part2(keys):
   global game_started
   minigame1_scroll = gamebox.from_image(400, 300, 'minigame1_scroll.png')
   minigame1_scroll.scale_by(0.8)
   camera.draw(background2)
   camera.draw(minigame1_scroll)
   if pygame.K_SPACE in keys:
       game_started = 2




# obstacles
books = gamebox.from_image(600, 500, 'books.png')
books.scale_by(0.09)
books.y = 400
books.speedx = -10

pencils = gamebox.from_image(0, 200, 'pencil.png')
pencils.scale_by(0.15)
pencils.y = 200

score = 0


# randomization


def books_randomize_height():
   '''
   randomizes the height of books
   :return: different heights of books
   '''
   books_height_scale = random.randint(70, 150)
   height = float(books_height_scale / 100)
   return height


def move_books():
   '''
   makes the books move across the camera
   :return: a moving books
   '''
   books.left = camera.right
   books.speedx = -10


def books_randomize_speed():
   '''
   randomizes the speed of books
   return: different speeds of books
   '''
   books_speed = random.randint(10, 20)
   return books_speed


def pencils_randomize_height():
   pencils_height = random.randint(70, 300)
   return pencils_height


def pencils_randomize_speed():
   """
   randomizes the speed of the pencils
   :return: new speed of pencils
   """
   pencils_speed = random.randint(10, 20)
   return pencils_speed


pencils.speedx = pencils_randomize_speed()
pencils.y = pencils_randomize_height()
pencils.move_speed()

r_pressed = False


def move_pencils():
   """
   makes the pencils move across the camera
   :return: pencils on the other side of the screen
   """
   pencils.right = camera.left


def draw_part2(keys):
   '''
   operates the whole game that will be called every frame
   :param keys: keys pressed (e.i space bar)
   :return: an action resulted by pressing the keys
   '''
   global score
   global books
   global floor2
   global sky
   global tick_count
   global game_started
   global pencils
   global mylives
   global r_pressed
   tick_count += 1

   player1.image = rightimages[0]

   if pygame.K_RIGHT in keys:
       player1.x += 10
       player1.image = rightimages[tick_count // 2 % len(rightimages)]
   if pygame.K_LEFT in keys:
       player1.standing = False
       player1.x -= 10
       player1.image = leftimages[tick_count // 2 % len(leftimages)]
   if pygame.K_UP in keys:
       player1.y -= 10
       player1.image = upimages[tick_count // 2 % len(upimages)]
   if pygame.K_DOWN in keys:
       player1.y += 10
       player1.image = downimages[tick_count // 2 % len(downimages)]
   if pygame.K_SPACE in keys:
       player1.speedy -= 4
       keys.clear()
   if player1.y < 400:
       player1.image = rightimages[1]

   # player stuff
   player1.speedy += 0.5
   player1.y += player1.speedy

   player1.move_speed()
   player1.move_to_stop_overlapping(floor2)
   player1.move_to_stop_overlapping(sky)

   # pencils and books
   if books.right < camera.left:
       score += 1
       scale = float(books_randomize_height())
       books.scale_by(scale)
       move_books()

   if pencils.left > camera.right:
       move_pencils()
       pencils.speedx = pencils_randomize_speed()
       pencils.y = pencils_randomize_height()
       pencils.move_speed()

   to_draw_part2 = [floor2, background2, player1, sky, books, pencils]
   for thing in to_draw_part2:
       camera.draw(thing)

   to_draw_lives = []

   for i in range(mylives):
       heart_i = gamebox.from_image(350 + (50 * i), 50, 'heart1.png')
       to_draw_lives.append(heart_i)
       for box in to_draw_lives:
           camera.draw(box)

   books.move_speed()
   pencils.move_speed()

   score_text = "Score: " + str(score) + ' points'
   score_board = gamebox.from_text(400, 10, str(score_text), 30, 'white')
   camera.draw(score_board)

   if player1.touches(books):
       mylives -= 1
       books_randomize_height()
       books_randomize_speed()
       pencils_randomize_height()
       pencils_randomize_speed()
       move_books()
       move_pencils()
       draw_part2(keys)

   if player1.touches(pencils):
       mylives -= 1
       books_randomize_height()
       books_randomize_speed()
       pencils_randomize_height()
       pencils_randomize_speed()
       move_books()
       move_pencils()
       draw_part2(keys)

   if mylives == 0:
       game_over_scroll = gamebox.from_image(400, 300, 'game_over_scroll.png')
       pencils.speedx = 0
       books.speedx = 0
       game_over_scroll.scale_by(0.8)
       camera.draw(game_over_scroll)

   if score == 1:
       game_started = 2.5


player1_above = gamebox.from_image(50, 40, 'player1_head.png')
player1_above.scale_by(0.08)
player1_above.speedx = 0
player1_above.speedy = 0

# drawing the maze
draw_maze = []
block1 = gamebox.from_color(485, 0, 'white', 690, 120)
draw_maze.append(block1)
block2 = gamebox.from_color(0, 310, 'white', 140, 500)
draw_maze.append(block2)
block3 = gamebox.from_color(800, 290, 'white', 140, 500)
draw_maze.append(block3)
block4 = gamebox.from_color(315, 600, 'white', 690, 120)
draw_maze.append(block4)
block5 = gamebox.from_color(95, 350, 'white', 70, 20)
draw_maze.append(block5)
block6 = gamebox.from_color(270, 150, 'white', 20, 220)
draw_maze.append(block6)
block7 = gamebox.from_color(480, 100, 'white', 20, 100)
draw_maze.append(block7)
block8 = gamebox.from_color(165, 150, 'white', 70, 20)
draw_maze.append(block8)
block9 = gamebox.from_color(130, 200, 'white', 20, 120)
draw_maze.append(block9)
block10 = gamebox.from_color(235, 250, 'white', 230, 20)
draw_maze.append(block10)
block11 = gamebox.from_color(200, 300, 'white', 20, 100)
draw_maze.append(block11)
block12 = gamebox.from_color(200, 450, 'white', 160, 20)
draw_maze.append(block12)
block13 = gamebox.from_color(270, 400, 'white', 20, 120)
draw_maze.append(block13)
block14 = gamebox.from_color(305, 350, 'white', 90, 20)
draw_maze.append(block14)
block15 = gamebox.from_color(340, 300, 'white', 20, 120)
draw_maze.append(block15)
block16 = gamebox.from_color(340, 500, 'white', 20, 120)
draw_maze.append(block16)
block17 = gamebox.from_color(480, 500, 'white', 20, 120)
draw_maze.append(block17)
block18 = gamebox.from_color(480, 450, 'white', 160, 20)
draw_maze.append(block18)
block19 = gamebox.from_color(410, 400, 'white', 20, 120)
draw_maze.append(block19)
block20 = gamebox.from_color(550, 400, 'white', 20, 120)
draw_maze.append(block20)
block21 = gamebox.from_color(550, 350, 'white', 160, 20)
draw_maze.append(block21)
block22 = gamebox.from_color(480, 300, 'white', 20, 120)
draw_maze.append(block22)
block23 = gamebox.from_color(445, 250, 'white', 90, 20)
draw_maze.append(block23)
block24 = gamebox.from_color(410, 200, 'white', 20, 120)
draw_maze.append(block24)
block25 = gamebox.from_color(375, 150, 'white', 90, 20)
draw_maze.append(block25)
block26 = gamebox.from_color(620, 250, 'white', 20, 220)
draw_maze.append(block26)
block27 = gamebox.from_color(620, 150, 'white', 160, 20)
draw_maze.append(block27)
block28 = gamebox.from_color(550, 200, 'white', 20, 120)
draw_maze.append(block28)
block29 = gamebox.from_color(670, 300, 'white', 20, 120)
draw_maze.append(block29)
block30 = gamebox.from_color(705, 250, 'white', 80, 20)
draw_maze.append(block30)
block31 = gamebox.from_color(690, 450, 'white', 120, 20)
draw_maze.append(block31)

zombieleftimages = gamebox.load_sprite_sheet('zombie_left.png', 1, 4)
zombierightimages = gamebox.load_sprite_sheet('zombies_right.png', 1, 4)

zombie1 = gamebox.from_image(100, 520, zombierightimages[0])
zombie2 = gamebox.from_image(560, 123, zombierightimages[0])
zombie3 = gamebox.from_image(510, 520, zombierightimages[0])

coin_list = [
   gamebox.from_image(510, 520, 'nickel.png'),
   gamebox.from_image(560, 140, 'nickel.png'),
   gamebox.from_image(205, 520, 'nickel.png')
]

coin_amount = 0


def draw_intro_part3(keys):
   global game_started
   minigame2_scroll = gamebox.from_image(400, 300, 'minigame2_scroll.png')
   minigame2_scroll.scale_by(0.8)
   camera.draw(minigame2_scroll)
   if pygame.K_SPACE in keys:
       game_started = 3


def draw_part3(keys):
   global game_started
   global tick_count
   global mylives
   global coin_amount

   to_draw_lives = []

   tick_count += 1

   if pygame.K_RIGHT in keys:
       player1_above.x += 10
   if pygame.K_LEFT in keys:
       player1_above.x -= 10
   if pygame.K_UP in keys:
       player1_above.y -= 10
   if pygame.K_DOWN in keys:
       player1_above.y += 10

   for thing in draw_maze:
       player1_above.move_to_stop_overlapping(thing)
       camera.draw(thing)

   if zombie1.x < 105:
       zombie1.speedx = 3
   if zombie1.x > 290:
       zombie1.speedx = -3

   if zombie2.x < 565:
       zombie2.speedx = 3
   if zombie2.x > 685:
       zombie2.speedx = -3

   if zombie3.x < 515:
       zombie3.speedx = 3
   if zombie2.x > 635:
       zombie3.speedx = -3

   if zombie1.speedx == 3:
       zombie1.image = zombierightimages[tick_count // 2 % len(zombierightimages)]
   else:
       zombie1.image = zombieleftimages[tick_count // 2 % len(zombieleftimages)]

   if zombie2.speedx == 3:
       zombie2.image = zombierightimages[tick_count // 2 % len(zombierightimages)]
   else:
       zombie2.image = zombieleftimages[tick_count // 2 % len(zombieleftimages)]

   if zombie3.speedx == 3:
       zombie3.image = zombierightimages[tick_count // 2 % len(zombierightimages)]
   else:
       zombie3.image = zombieleftimages[tick_count // 2 % len(zombieleftimages)]

   zombie1.move_speed()
   zombie2.move_speed()
   zombie3.move_speed()

   camera.draw(player1_above)

   camera.draw(zombie1)
   camera.draw(zombie2)
   camera.draw(zombie3)

   if player1_above.touches(zombie1):
       mylives -= 1

   if player1_above.touches(zombie2):
       mylives -= 1

   if player1_above.touches(zombie3):
       mylives -= 1

   for i in range(mylives):
       heart_i = gamebox.from_image(350 + (50 * i), 20, 'heart1.png')
       to_draw_lives.append(heart_i)
       for box in to_draw_lives:
           camera.draw(box)

   if mylives == 0:
       game_over_scroll = gamebox.from_image(400, 300, 'game_over_scroll.png')
       game_over_scroll.scale_by(0.8)
       camera.draw(game_over_scroll)
       gamebox.pause()

   for coin in coin_list:
       camera.draw(coin)

   for coin in coin_list:
       if player1_above.touches(coin):
           coin_list.remove(coin)
           coin_amount += 1

   if coin_amount == 3:
       game_started = 3.5

   if mylives == 0:
       game_over_scroll = gamebox.from_image(400, 300, 'game_over_scroll.png')
       game_over_scroll.scale_by(0.8)
       camera.draw(game_over_scroll)
       gamebox.pause()


# dartgame
cross = gamebox.from_image(400, 300, 'cross.png')

camera.draw(background3)

target = gamebox.from_image(0, 300, 'target.png')
target.right = camera.left
target.y = 300
target.speedx = 12

basketball = None

# Variables
hit_count = 0  # number bullseyes hit
miss_count = 0  # number bullseyes that you miss

basketball_life = 15  # number of frames until basketball hits
basketball_age = 0  # number of frames the basketball has existed

# to make sure the mouse is released between clicks
# says that the previous frame had a mouse click
held_click = False


def draw_intro_part4(keys):
   global game_started
   minigame3_scroll = gamebox.from_image(400, 300, 'minigame3_scroll.png')
   minigame3_scroll.scale_by(0.8)
   camera.draw(background3)
   camera.draw(minigame3_scroll)
   if pygame.K_SPACE in keys:
       game_started = 4


def reset_target():
   """
   places the target on a random side of the screen at a random y position
   """
   previously_right = target.speedx > 0
   target.x = random.randint(0, 1) * 600
   now_right = target.x < 300
   if previously_right != now_right:
       target.speedx = -target.speedx
       target.flip()
   target.y = random.randint(100, 500)


def basketball_speed():
   """
   Sets the speed of the basketball so that it will hit on target
   """
   curr_x = basketball.x
   curr_y = basketball.y
   dest_x = cross.x
   dest_y = cross.y
   diff_x = dest_x - curr_x
   diff_y = dest_y - curr_y
   basketball.speedx = diff_x / basketball_life
   basketball.speedy = diff_y / basketball_life


def draw_part4(keys):
   global game_started
   global tick_count
   global basketball
   global basketball_age
   global hit_count
   global miss_count
   global held_click

   to_draw = []

   # Background
   to_draw.append(background3)

   tick_count += 1
   target.move_speed()

   if target.left > camera.right or target.right < camera.left:
       reset_target()
       miss_count += 1  # we we wrapped, then the target got away


   to_draw.append(target)

   if pygame.K_RIGHT in keys:
       cross.x += 20
   if pygame.K_LEFT in keys:
       cross.x -= 20
   if pygame.K_UP in keys:
       cross.y -= 20
   if pygame.K_DOWN in keys:
       cross.y += 20

   to_draw.append(cross)

   if pygame.K_SPACE in keys and basketball is None:
       basketball = gamebox.from_image(300, 600, 'basketball.png')
       basketball_speed()  # Sets the basketball's speed

   # What to do when an basketball hits
   if basketball_age > basketball_life:
       if basketball.touches(target):
           reset_target()
           hit_count += 1
       basketball = None
       basketball_age = 0

   if basketball is not None:
       # We only draw, move, age the basketball if we have one
       to_draw.append(basketball)
       basketball.move_speed()
       basketball_age += 1

   # Scoreboard, which we want to be drawn last
   score_text = str(hit_count) + ' Hit | ' + str(miss_count) + ' Missed'
   score_board = gamebox.from_text(400, 10, score_text, 30, 'white')
   to_draw.append(score_board)

   for box in to_draw:
       camera.draw(box)
   if miss_count == 5:
       game_over_scroll = gamebox.from_image(400, 300, 'game_over_scroll.png')
       game_over_scroll.scale_by(0.8)
       camera.draw(game_over_scroll)
       gamebox.pause()

   if hit_count == 5:
       game_started = 5


def draw_part5():
   global game_started
   end_scroll = gamebox.from_image(400, 300, 'end_scroll.png')
   end_scroll.scale_by(0.8)
   ending_image = gamebox.from_image(400, 300, 'winningimage.jpg')
   camera.draw(ending_image)
   camera.draw(end_scroll)


def tick(keys):
   camera.clear('black')
   if game_started == 0:
       draw_title(keys)
   elif game_started == 1:
       draw_part1(keys)
   elif game_started == 1.5:
       draw_intro_part2(keys)
   elif game_started == 2:
       draw_part2(keys)
   elif game_started == 2.5:
       draw_intro_part3(keys)
   elif game_started == 3:
       draw_part3(keys)
   elif game_started == 3.5:
       draw_intro_part4(keys)
   elif game_started == 4:
       draw_part4(keys)
   elif game_started == 5:
       draw_part5()
   camera.display()


gamebox.timer_loop(30, tick)


