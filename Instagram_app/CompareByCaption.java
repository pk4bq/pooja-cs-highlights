/**
 * Pooja Kaji (pk4bq) 
 * Sources :Big Java book, Professor Hott's Slides on Collab, and TA and Professor Hott's
 * office Hours
 */

import java.util.Comparator;

public class CompareByCaption implements Comparator<Photograph> {
    /**
     * Compares captions of 2 photos.
     * If two captions are identical, then compare by rating, in descending order with the highest-rated photo first.
     * 
     * @param Photograph p1 and Photograph p2
     * @retrun a postive or negative integer or 0 (if comparing by rating)
     */
    public int compare(Photograph p1, Photograph p2) {
        if (p1.getCaption().compareTo(p2.getCaption()) == 0) {
            if (p2.getRating() > p1.getRating()) {
                return 1;
            }
            if (p2.getRating() < p1.getRating()) {
                return -1;
            } else {
                return 0;
            }
        }
        if ((p1.getCaption().compareTo(p2.getCaption()) > 0)) {
            return 1;
        } else { // caption compareTo of p1 and p2 is less than 0
            return -1;
        }
    }
}
