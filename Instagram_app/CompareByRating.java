/**
 * Pooja Kaji 
 * pk4bq 
 * Sources :Big Java book, Professor Hott's Slides on Collab, and TA and Professor Hott's
 * office Hours
 */

import java.util.Comparator;
public class CompareByRating implements Comparator <Photograph> {
    /**
     * Compares rating of 2 photos in descending order. If two ratings are identical, 
     * then compare by caption in alphabetical order.
     * 
     * @param Photograph p1 and Photograph p2
     * @retrun a postive or negative integer or 0 (if the captions are equals to one another) 
     */

    public int compare(Photograph p1, Photograph p2) {
        if (p2.getRating() > p1.getRating()) {
            return 1;
        }
        if (p2.getRating() < p1 .getRating()) {
            return -1;
        } 
        if (p2.getRating() == p1 .getRating()){
            if (p1.getCaption().compareTo(p2.getCaption()) > 0){ //returns a positive number
                return 1; 
            }if (p1.getCaption().compareTo(p2.getCaption()) < 0){ //returns a negative number 
                return -1;
            }
        }
        return 0;
    }
}

