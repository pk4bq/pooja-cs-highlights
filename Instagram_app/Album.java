/**
 * Pooja Kaji (pk4bq) 
 * Sources :Big Java book, Professor Hott's Slides on Collab, and TA and Professor Hott's
 * office Hours
 */

import java.util.ArrayList;

public class Album extends PhotographContainer {
    /**
     * Instance name and photos are created
     */
    public Album(String name) {
        super(name);
        this.photos = new ArrayList<Photograph>();
        
     
    }

}
