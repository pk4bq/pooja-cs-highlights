import java.util.ArrayList;
import java.util.HashSet;

/**
 * Pooja Kaji (pk4bq) 
 * Sources :Big Java book, Professor Hott's Slides on Collab, and TA and Professor Hott's
 * office Hours
 */

abstract public class PhotographContainer {

    /**
     * Holds the name and photos of a PhotographContainer
     */
    protected String name;
    protected ArrayList<Photograph> photos = new ArrayList<Photograph>();

    /**
     * Constructor Instances name and photos
     */
    public PhotographContainer(String name) {
        this.name = name;
        this.photos = new ArrayList<Photograph>();
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the photos
     */
    public ArrayList<Photograph> getPhotos() {
        return photos;
    }

    // other methods
    /**
     * Adds the Photograph p to the list of the current object’s photos feed if and only if it was not already in that list.
     * 
     * @param Photograph p
     * @return true if the Photograph was added; @return false if it was not added.
     */

    public boolean addPhoto(Photograph p) {
        if (p == null) {
            return false;
        }
        if (photos.contains(p)) {
            return false;
        } else {
            photos.add(p);
            return true;
        }
    }

    /**
     * @param Photograph p
     * @return true if the current object has p in its list of photos; other wise @return false.
     */

    public boolean hasPhoto(Photograph p) {
        if (photos.contains(p)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * If Photograph p is in the current Container object’s list of Photographs, removes p from the current object’s list.
     * 
     * @param Photograph p
     * @returns true if the Photograph was removed or false if it was not there.
     */

    public boolean removePhoto(Photograph p) {
        if (photos.contains(p)) {
            photos.remove(p);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return the number of Photographs in the Photograph Container.
     */

    public int numPhotographs() {
        int size = photos.size();
        return size;
    }

    /**
     * Given an object o, return true if the current Container object’s name is equal to the name of the Container object
     * passed to equals(). Otherwise, return false.
     * 
     * @param Object o
     * @return whether or not the Container object’s name is equal to the name of the Container object
     */

    public boolean equals(Object o) {
        if (o == null) { // o is null
            return false;
        }
        if (!(o instanceof PhotographContainer)) {
            return false; // o is not an Photograph Container
        }
        PhotographContainer oPhotographContainer = (PhotographContainer) o; // Re-cast o to a Photograph Container
        if (this.name == oPhotographContainer.getName()) {
            return true;
        } else {
            return false; // o has wrong Photograph Container
        }
    }

    /**
     * Generates a String that shows the values of the fields name (name of container) and photos.
     * 
     * @return string
     */

    public String toString() {
        return "(Name: " + this.name + ", " + "Photos: " + this.photos + ")";
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    /**
     * Return an ArrayList of photos from the photos feed that have a rating greater than or equal to the given parameter.
     * If the rating is incorrectly formatted, return null. If there are no photos of that rating or higher, return an empty
     * ArrayList.
     * 
     * @param rating between integers 1 and 5
     * @returns ArrayList of photos that have that rating or higher or null if improperly formatted
     */
    public ArrayList<Photograph> getPhotos(int rating) {
        ArrayList<Photograph> bestphotos = new ArrayList<Photograph>();
        if (rating < 0 || rating > 5) { // rating as to be between 0 and 5 so this is an exception
            return null;
        }
        for (Photograph i : photos) {
            if (i.getRating() >= rating) {
                bestphotos.add(i);
            }
        }
        return bestphotos;
    }

    /**
     * Return an ArrayList of photos from the photos feed that were taken in the year provided. If the year is incorrectly
     * formatted, return null. If there are no photos taken that year, return an empty ArrayList.
     * 
     * @param year photo was taken as an integer
     * @returns ArrayList of photos taken in that year or null if improperly formatted
     */
    public ArrayList<Photograph> getPhotosInYear(int year) {
        ArrayList<Photograph> newphotos = new ArrayList<Photograph>();
        String tempyear = Integer.toString(year); // converts a string to an int
        if (tempyear.length() != 4) {
            return null;
        } else {
            for (Photograph photos : this.photos) {
                String newyear = photos.getDateTaken();
                int currentyear = Integer.parseInt(newyear.substring(0, 4));
                if (year == currentyear) {
                    newphotos.add(photos);
                }
            }
            return newphotos;
        }
    }

    /**
     * Return an ArrayList of photos from the photos feed that were taken in the month and year provided. If the month or
     * year are incorrectly formatted, return null. If there are no photos taken that month, return an empty ArrayList.
     * 
     * @param a month and year which are integers
     * @returns an arraylist of photos that were taken in that month or year or null if improperly formatted
     */
    public ArrayList<Photograph> getPhotosInMonth(int month, int year) {
        ArrayList<Photograph> newphotos = new ArrayList<Photograph>();
        String tempyear = Integer.toString(year);
        if (tempyear.length() != 4 || month >= 13) { // an exception
            return null;
        } else {
            for (Photograph photos : this.photos) {
                String newyear = photos.getDateTaken();
                String newmonth = photos.getDateTaken();
                int currentyear = Integer.parseInt(newyear.substring(0, 4));
                int currentmonth = Integer.parseInt(newmonth.substring(5, 7));
                if (year == currentyear && month == currentmonth) {
                    newphotos.add(photos);
                }
            }
            return newphotos;

        }
    }

    /**
     * Return an ArrayList of photos from the photos feed that were taken between beginDate and endDate (inclusive).If the
     * begin and end dates are incorrectly formatted, or beginDate is after endDate, return null. If there are no photos
     * taken during the period, return an empty ArrayList.
     * 
     * @param Beginning and end date of photos taken as Strings
     * @returns an ArrayList of photos that were taken between thoses dates, inclusive or null if improperly formatted
     */

    public ArrayList<Photograph> getPhotosBetween(String beginDate, String endDate) {
        ArrayList<Photograph> newphotos = new ArrayList<Photograph>();

        if (beginDate.length() < 10 || endDate.length() < 10) {
            return null;
        }

        int yearInt1 = Integer.parseInt(beginDate.substring(0, 4));
        int monthInt1 = Integer.parseInt(beginDate.substring(5, 7));
        int dayInt1 = Integer.parseInt(beginDate.substring(8));
        int yearInt2 = Integer.parseInt(endDate.substring(0, 4));
        int monthInt2 = Integer.parseInt(endDate.substring(5, 7));
        int dayInt2 = Integer.parseInt(endDate.substring(8));

        if (beginDate.charAt(4) != '-' || beginDate.charAt(7) != '-') {
            return null;
        }

        if (Integer.toString(yearInt1).length() != 4 || Integer.toString(yearInt2).length() != 4) {
            return null;
        }
        for (int i = 0; i < beginDate.length(); i++) { // check to see the placement of the dashes
            if (i != 4 && i != 7) {
                if (beginDate.charAt(i) == '-') {
                    return null;
                }
            }
        }

        for (int i = 0; i < endDate.length(); i++) {
            if (i != 4 && i != 7) {
                if (endDate.charAt(i) == '-') {
                    return null;
                }
            }
        }
        if (monthInt1 < 1 || monthInt1 > 12 || monthInt2 < 1 || monthInt2 > 12) {
            return null;
        }

        if (monthInt1 == 2) {
            if (dayInt1 < 1 || dayInt1 > 28) {
                return null;
            }
        }

        if (monthInt2 == 2) {
            if (dayInt2 < 1 || dayInt2 > 28) {
                return null;
            }
        }

        if (monthInt1 == 1 || monthInt1 == 3 || monthInt1 == 5 || monthInt1 == 7 || monthInt1 == 9 || monthInt1 == 10
                || monthInt1 == 12) {
            if (dayInt1 < 1 || dayInt1 > 31) {
                return null;
            }
        }

        if (monthInt2 == 1 || monthInt2 == 3 || monthInt2 == 5 || monthInt2 == 7 || monthInt2 == 9 || monthInt2 == 10
                || monthInt2 == 12) {
            if (dayInt2 < 1 || dayInt2 > 31) {
                return null;
            }
        }

        if (monthInt1 == 4 || monthInt1 == 6 || monthInt1 == 9 || monthInt1 == 11) {
            if (dayInt1 < 1 || dayInt1 > 30) {
                return null;
            }
        }

        if (monthInt2 == 4 || monthInt2 == 6 || monthInt2 == 9 || monthInt2 == 11) {
            if (dayInt2 < 1 || dayInt2 > 30) {
                return null;
            }
        }

        if (yearInt1 > yearInt2) {
            return null;
        }

        if (yearInt1 == yearInt2 && monthInt1 > monthInt2) {
            return null;
        }

        if (yearInt1 == yearInt2 && monthInt1 == monthInt2 && dayInt1 > dayInt2) {
            return null;
        }

        for (Photograph p : this.photos) { // this for loop checks each of the if and else statements below for each photo in the
            // list of photos
            String myYear = p.getDateTaken().substring(0, 4);
            String myMonth = p.getDateTaken().substring(5, 7);
            String myDay = p.getDateTaken().substring(8);
            int coolYear = Integer.parseInt(myYear);
            int coolMonth = Integer.parseInt(myMonth);
            int coolDay = Integer.parseInt(myDay);

            if (coolYear > yearInt1 && coolYear < yearInt2) {
                newphotos.add(p);

            } else if (coolYear > yearInt1 && coolYear == yearInt2) {
                if (coolMonth < monthInt2) {
                    newphotos.add(p);
                } else if (coolMonth == monthInt2) {
                    if (coolDay <= dayInt2) {
                        newphotos.add(p);
                    }
                }
            } else if (coolYear == yearInt1 && coolYear < yearInt2) {
                if (coolMonth > monthInt1) {
                    newphotos.add(p);
                } else if (coolMonth == monthInt1) {
                    if (coolDay >= dayInt1) {
                        newphotos.add(p);
                    }
                }
            } else if (coolYear == yearInt1 && coolYear == yearInt2) {
                if (coolMonth >= monthInt1 && coolMonth <= monthInt2) {
                    if (coolDay >= dayInt1 && coolDay <= dayInt2) {
                        newphotos.add(p);
                    }
                }
            }
        }
        return newphotos;
    }
}
