/**
 * Pooja Kaji (pk4bq) 
 * Sources :Big Java book, Java API(see below), Professor Hott's Slides on Collab, and TA office hours
 * This is a GUI interface that mimics the instagram application. It interacts with the other files in the zip. 
 */

//imports 
import java.util.ArrayList;
import java.util.Collections;
import java.awt.*;
import java.awt.event.*;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;

public class PhotoViewer extends JFrame {

    //fields
    private static PhotographContainer imageAlbum;
    private FlowLayout layout = new FlowLayout();
    private JButton previous;
    private JButton next;
    private JButton exit;
    private JButton Date;
    private JButton Rating;
    private JButton Caption;
    private JRadioButton one;
    private JRadioButton two;
    private JRadioButton three;
    private JRadioButton four;
    private JRadioButton five;
    private int rating1;
    private int rate;
    private JLabel rating;
    private JLabel sort;
    private JLabel mainpic;
    private JLabel label;
    private JLabel label2;
    private JLabel label3;
    private JLabel label4;
    private JLabel label5;
    private ArrayList<ImageIcon> morepics;
    private ArrayList<ImageIcon> pics;
    private String captiona;
    private String captionb;
    private String captionc;
    private String captiond;
    private String captione;
    private String datea;
    private String  dateb;
    private String datec;
    private String dated;
    private String datee;
    private String ratinga;
    private String ratingb;
    private String ratingc;
    private String ratingd;
    private String ratinge;
    private JPanel panel1;
    private ImageIcon icon1 = new ImageIcon("images/Image5.jpg");; // https://www.leepoint.net/GUI-lowlevel/graphics/45imageicon.html
    private ImageIcon icon2 = new ImageIcon("images/Image4.jpg");
    private ImageIcon icon3 = new ImageIcon("images/Image3.jpg");
    private ImageIcon icon4 = new ImageIcon("images/Image1.jpg");
    private ImageIcon icon5 = new ImageIcon("images/Image2.jpg");
    private ImageIcon icon1a = new ImageIcon("images/Image5.jpg");; // https://www.leepoint.net/GUI-lowlevel/graphics/45imageicon.html
    private ImageIcon icon2b = new ImageIcon("images/Image4.jpg");
    private ImageIcon icon3c = new ImageIcon("images/Image3.jpg");
    private ImageIcon icon4d = new ImageIcon("images/Image1.jpg");
    private ImageIcon icon5e = new ImageIcon("images/Image2.jpg");

    //methods
    public void addComponentsToPane(Container pane) {
        /**
         * This method creates all my panels, labels, icons, and buttons through a container pane parameter
         * 
         */
        // PANEL 1
        panel1 = new JPanel();
        // add a layout
        panel1.setLayout(layout); // flow layout
        layout.setAlignment(FlowLayout.CENTER);  
        panel1.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

        // PANEL 2
        // buttons
        JPanel panel2 = new JPanel();
        panel2.setBackground(Color.GREEN);//sets the color
        previous = new JButton("Previous");//creates JButton called previous
        panel2.add(previous);
        previous.setActionCommand("Previous");//creates an action for that command 
        next = new JButton("Next");//creates JButton called next
        panel2.add(next);
        next.setActionCommand("Next");
        exit = new JButton("Exit");//creates JButton called exit
        panel2.add(exit);
        previous.setActionCommand("Exit");
        exit.setActionCommand("Exit");
        exit.addActionListener(new ButtonListener());
        previous.setActionCommand("Previous");
        previous.addActionListener(new ButtonListener());
        next.setActionCommand("Next");
        next.addActionListener(new ButtonListener());

        // PANEL 3
        JPanel panel3 = new JPanel();
        // RadioButtons
        rating = new JLabel("Rate this Picture from 1 to 5!");
        panel3.setBackground(Color.PINK);
        panel3.add(rating);
        one = new JRadioButton("1");//https://docs.oracle.com/javase/tutorial/uiswing/components/button.html
        two = new JRadioButton("2");
        three = new JRadioButton("3");
        four = new JRadioButton("4");
        five = new JRadioButton("5");
        one.setActionCommand("1");
        two.setActionCommand("2");
        three.setActionCommand("3");
        four.setActionCommand("4");
        five.setActionCommand("5");
        ButtonGroup cool = new ButtonGroup();//creates a button group so that only one button is selected and the others are not
        cool.add(one);
        cool.add(two);
        cool.add(three);
        cool.add(four);
        cool.add(five);
        panel3.add(one);
        panel3.add(two);
        panel3.add(three);
        panel3.add(four);
        panel3.add(five);
        one.addActionListener(new ButtonListener());//implements an action when a button is pressed 
        two.addActionListener(new ButtonListener());
        three.addActionListener(new ButtonListener());
        four.addActionListener(new ButtonListener());
        five.addActionListener(new ButtonListener());


        // PANEL 4
        JPanel panel4 = new JPanel();
        // Date, Rating, and Caption Button
        sort = new JLabel("Sort by:");
        panel4.setBackground(Color.CYAN);
        panel4.add(sort);
        Date = new JButton("Date");
        panel4.add(Date);
        Date.setActionCommand("Date");
        Rating = new JButton("Rating");
        panel4.add(Rating);
        Rating.setActionCommand("Rating");
        Caption = new JButton("Caption");
        panel4.add(Caption);
        Caption.setActionCommand("Caption");
        Date.addActionListener(new ButtonListener());//https://docs.oracle.com/javase/tutorial/uiswing/components/button.html
        Rating.addActionListener(new ButtonListener());
        Caption.addActionListener(new ButtonListener());


        //PANEL 5
        //My thumbnails for my images

        //this is default setting for caption, rating, and date taken --> sorted by caption
        captiona = imageAlbum.photos.get(0).getCaption();//gets photo at index of 0 gives caption of first image
        captionb = imageAlbum.photos.get(1).getCaption();//gets photo at index of 1 gives caption of second image
        captionc = imageAlbum.photos.get(2).getCaption();
        captiond = imageAlbum.photos.get(3).getCaption();
        captione = imageAlbum.photos.get(4).getCaption();


        datea = imageAlbum.photos.get(0).getDateTaken();//gets photo at index of 0 gives date of first image
        dateb = imageAlbum.photos.get(1).getDateTaken();
        datec = imageAlbum.photos.get(2).getDateTaken();
        dated = imageAlbum.photos.get(3).getDateTaken();
        datee = imageAlbum.photos.get(4).getDateTaken();

        ratinga = imageAlbum.photos.get(0).getRating() + "";//gets photo at index of 0 gives rating of first image
        ratingb = imageAlbum.photos.get(1).getRating() + "";
        ratingc = imageAlbum.photos.get(2).getRating() + "";
        ratingd = imageAlbum.photos.get(3).getRating() + "";
        ratinge = imageAlbum.photos.get(4).getRating() + "";


        JPanel panel5 = new JPanel();
        label = new JLabel(icon1);
        mainpic = new JLabel(icon1a);//creates a new label called mainpic 
        icon1 =  (new ImageIcon(new ImageIcon("images/image5.jpg").getImage().getScaledInstance(100, 71, Image.SCALE_DEFAULT)));//sets image to icon and small image scale
        icon1a = (new ImageIcon(new ImageIcon("images/Image5.jpg").getImage().getScaledInstance(900, 643, Image.SCALE_DEFAULT)));//sets image to icon and large image scale 
        label.setIcon(icon1);//adds icon to label
        mainpic.setIcon(icon1a);//adds iconla to mainpic label (sets the center/large picture)
        label.setText("<html>" + captiona + "<br/>" + datea + "<<br/>"+ ratinga + "</html>");


        label2 = new JLabel(icon2);
        icon2 =  (new ImageIcon(new ImageIcon("images/Image4.jpg").getImage().getScaledInstance(100, 71, Image.SCALE_DEFAULT)));
        icon2b = (new ImageIcon(new ImageIcon("images/Image4.jpg").getImage().getScaledInstance(900, 643, Image.SCALE_DEFAULT)));
        label2.setIcon(icon2);
        label2.setText("<html>" + captionb + "<br/>" + dateb + "<<br/>"+ ratingb + "</html>");


        label3 = new JLabel(icon3);
        icon3 =  (new ImageIcon(new ImageIcon("images/Image3.jpg").getImage().getScaledInstance(100, 71, Image.SCALE_DEFAULT)));
        icon3c = (new ImageIcon(new ImageIcon("images/Image3.jpg").getImage().getScaledInstance(900, 643, Image.SCALE_DEFAULT)));
        label3.setIcon(icon3);
        label3.setText("<html>" + captionc + "<br/>" + datec + "<<br/>"+ ratingc + "</html>");


        label4 = new JLabel(icon4);
        icon4  =  (new ImageIcon(new ImageIcon("images/Image1.jpg").getImage().getScaledInstance(100, 71, Image.SCALE_DEFAULT)));
        icon4d = (new ImageIcon(new ImageIcon("images/Image1.jpg").getImage().getScaledInstance(900, 643, Image.SCALE_DEFAULT)));
        label4.setIcon(icon4);
        label4.setText("<html>" + captiond + "<br/>" + dated + "<<br/>"+ ratingd + "</html>"); 


        label5 = new JLabel(icon5);
        icon5 =  (new ImageIcon(new ImageIcon("images/Image2.jpg").getImage().getScaledInstance(100, 71, Image.SCALE_DEFAULT)));
        icon5e = (new ImageIcon(new ImageIcon("images/Image2.jpg").getImage().getScaledInstance(900, 643, Image.SCALE_DEFAULT)));
        label5.setIcon(icon5);
        label5.setText("<html>" + captione + "<br/>" + datee + "<<br/>"+ ratinge + "</html>");

        panel5.add(label);
        panel5.add(label2);
        panel5.add(label3);
        panel5.add(label4);
        panel5.add(label5);
        panel5.add(mainpic);
        //this creates a vertical box so that my thumbnails are in columns 
        Box box = Box.createVerticalBox(); //TA Help 
        box.add(label);
        box.add(Box.createVerticalStrut(10));
        box.add(label2);
        box.add(Box.createVerticalStrut(10));
        box.add(label3);
        box.add(Box.createVerticalStrut(10));
        box.add(label4);
        box.add(Box.createVerticalStrut(10));
        box.add(label5);
        panel5.add(box);

        //border layout
        pane.add(panel1, BorderLayout.NORTH);// top
        pane.add(panel2, BorderLayout.SOUTH);// bottom 
        pane.add(panel3, BorderLayout.NORTH);// top
        pane.add(panel4, BorderLayout.WEST); // left
        pane.add(panel5, BorderLayout.EAST); // right


        //mouse listener
        label.addMouseListener(new Click());
        label2.addMouseListener(new Click());
        label3.addMouseListener(new Click());
        label4.addMouseListener(new Click());
        label5.addMouseListener(new Click());

        //adds icons in an ArrayList in the correct order
        pics = new ArrayList <ImageIcon>();
        pics.add(icon1);
        pics.add(icon2);
        pics.add(icon3);
        pics.add(icon4);
        pics.add(icon5);

        //adds icons in an ArrayList in the correct order
        morepics = new ArrayList <ImageIcon>();
        morepics.add(icon1a);
        morepics.add(icon2b);
        morepics.add(icon3c);
        morepics.add(icon4d);
        morepics.add(icon5e);


        //this changes the rating on demand
        //https://docs.oracle.com/javase/tutorial/uiswing/components/button.html
        rating1 = morepics.indexOf(mainpic.getIcon());//an integer is equal to the index value of the main picture 
        rating1 = imageAlbum.photos.get(rating1).getRating();//gets the rating of that photo in the photocontainer 
        if(rating1 == 1) {
            one.setSelected(true);
        }if(rating1 == 2) {
            two.setSelected(true);
        }if(rating1 == 3) {
            three.setSelected(true);
        }if(rating1 == 4) {
            four.setSelected(true);
        }if(rating1 == 5) {
            five.setSelected(true);
        }



    }

    private class ButtonListener implements ActionListener {
        /**
         * ButtonListener class is activated --> whenever you press each button, some action occurs  
         * 
         */
        @Override
        public void actionPerformed(ActionEvent e) { //https:stackoverflow.com/questions/22286695/create-an-exit-button-in-java?rq=1
            if (e.getActionCommand().equals("Exit")) {// exits the window if you hit the exit button
                System.exit(0);
            } if(e.getActionCommand().equals("Previous")) {//goes to previous photo
                mainpic.setIcon((Icon)morepics.get(((morepics.indexOf(mainpic.getIcon()) - 1) + 5) % 5));//https://stackoverflow.com/questions/14785443/is-there-an-expression-using-modulo-to-do-backwards-wrap-around-reverse-overfl
                rating1 = morepics.indexOf(mainpic.getIcon());//an integer is equal to the index value of the main picture 
                rating1 = imageAlbum.photos.get(rating1).getRating();//gets the rating of that photo in the photocontainer 
                if(rating1 == 1) {
                    one.setSelected(true);//would have it selected on the GUI if true and if the rating of that image was 1
                }if(rating1 == 2) {
                    two.setSelected(true);
                }if(rating1 == 3) {
                    three.setSelected(true);
                }if(rating1 == 4) {
                    four.setSelected(true);
                }if(rating1 == 5) {
                    five.setSelected(true);
                }
            } if(e.getActionCommand().equals("Next")) { //goes to next photo
                mainpic.setIcon((Icon)morepics.get(((morepics.indexOf(mainpic.getIcon()) + 1) % 5)));//https://stackoverflow.com/questions/14785443/is-there-an-expression-using-modulo-to-do-backwards-wrap-around-reverse-overfl
                rating1 = morepics.indexOf(mainpic.getIcon());
                rating1 = imageAlbum.photos.get(rating1).getRating();
                if(rating1 == 1) {
                    one.setSelected(true);
                }if(rating1 == 2) {
                    two.setSelected(true);
                }if(rating1 == 3) {
                    three.setSelected(true);
                }if(rating1 == 4) {
                    four.setSelected(true);
                }if(rating1 == 5) {
                    five.setSelected(true);
                }
            } if(e.getActionCommand().equals("1")) {//if you press the JButton "1"
                rate = morepics.indexOf(mainpic.getIcon());//index of mainpic
                imageAlbum.photos.get(rate).setRating(1);//set that rating to the value you selected
                ratinga = imageAlbum.photos.get(0).getRating() + "";//index of 0 gives rating of first image
                ratingb = imageAlbum.photos.get(1).getRating() + "";
                ratingc = imageAlbum.photos.get(2).getRating() + "";
                ratingd = imageAlbum.photos.get(3).getRating() + "";
                ratinge = imageAlbum.photos.get(4).getRating() + "";

                //have to reset the text of the labels 
                label.setText("<html>" + captiona + "<br/>" + datea + "<<br/>"+ ratinga + "</html>");
                label2.setText("<html>" + captionb + "<br/>" + dateb + "<<br/>"+ ratingb + "</html>");
                label3.setText("<html>" + captionc + "<br/>" + datec + "<<br/>"+ ratingc + "</html>");
                label4.setText("<html>" + captiond + "<br/>" + dated + "<<br/>"+ ratingd + "</html>");
                label5.setText("<html>" + captione + "<br/>" + datee + "<<br/>"+ ratinge + "</html>");
            }

            if(e.getActionCommand().equals("2")) {

                rate = morepics.indexOf(mainpic.getIcon());
                imageAlbum.photos.get(rate).setRating(2);
                ratinga = imageAlbum.photos.get(0).getRating() + "";//index of 0 gives rating of first image
                ratingb = imageAlbum.photos.get(1).getRating() + "";
                ratingc = imageAlbum.photos.get(2).getRating() + "";
                ratingd = imageAlbum.photos.get(3).getRating() + "";
                ratinge = imageAlbum.photos.get(4).getRating() + "";

                //have to reset the text of the labels
                label.setText("<html>" + captiona + "<br/>" + datea + "<<br/>"+ ratinga + "</html>");
                label2.setText("<html>" + captionb + "<br/>" + dateb + "<<br/>"+ ratingb + "</html>");
                label3.setText("<html>" + captionc + "<br/>" + datec + "<<br/>"+ ratingc + "</html>");
                label4.setText("<html>" + captiond + "<br/>" + dated + "<<br/>"+ ratingd + "</html>");
                label5.setText("<html>" + captione + "<br/>" + datee + "<<br/>"+ ratinge + "</html>");
            }

            if(e.getActionCommand().equals("3")) {

                rate = morepics.indexOf(mainpic.getIcon());
                imageAlbum.photos.get(rate).setRating(3);
                ratinga = imageAlbum.photos.get(0).getRating() + "";//index of 0 gives rating of first image
                ratingb = imageAlbum.photos.get(1).getRating() + "";
                ratingc = imageAlbum.photos.get(2).getRating() + "";
                ratingd = imageAlbum.photos.get(3).getRating() + "";
                ratinge = imageAlbum.photos.get(4).getRating() + "";

                //have to reset the text of the labels
                label.setText("<html>" + captiona + "<br/>" + datea + "<<br/>"+ ratinga + "</html>");
                label2.setText("<html>" + captionb + "<br/>" + dateb + "<<br/>"+ ratingb + "</html>");
                label3.setText("<html>" + captionc + "<br/>" + datec + "<<br/>"+ ratingc + "</html>");
                label4.setText("<html>" + captiond + "<br/>" + dated + "<<br/>"+ ratingd + "</html>");
                label5.setText("<html>" + captione + "<br/>" + datee + "<<br/>"+ ratinge + "</html>");
            }

            if(e.getActionCommand().equals("4")) {

                rate = morepics.indexOf(mainpic.getIcon());
                imageAlbum.photos.get(rate).setRating(4);
                ratinga = imageAlbum.photos.get(0).getRating() + "";//index of 0 gives rating of first image
                ratingb = imageAlbum.photos.get(1).getRating() + "";
                ratingc = imageAlbum.photos.get(2).getRating() + "";
                ratingd = imageAlbum.photos.get(3).getRating() + "";
                ratinge = imageAlbum.photos.get(4).getRating() + "";

                //have to reset the text of the labels
                label.setText("<html>" + captiona + "<br/>" + datea + "<<br/>"+ ratinga + "</html>");
                label2.setText("<html>" + captionb + "<br/>" + dateb + "<<br/>"+ ratingb + "</html>");
                label3.setText("<html>" + captionc + "<br/>" + datec + "<<br/>"+ ratingc + "</html>");
                label4.setText("<html>" + captiond + "<br/>" + dated + "<<br/>"+ ratingd + "</html>");
                label5.setText("<html>" + captione + "<br/>" + datee + "<<br/>"+ ratinge + "</html>");
            }
            if(e.getActionCommand().equals("5")) {

                rate = morepics.indexOf(mainpic.getIcon());
                imageAlbum.photos.get(rate).setRating(5);
                ratinga = imageAlbum.photos.get(0).getRating() + "";//index of 0 gives rating of first image
                ratingb = imageAlbum.photos.get(1).getRating() + "";
                ratingc = imageAlbum.photos.get(2).getRating() + "";
                ratingd = imageAlbum.photos.get(3).getRating() + "";
                ratinge = imageAlbum.photos.get(4).getRating() + "";

                //have to reset the text of the labels
                label.setText("<html>" + captiona + "<br/>" + datea + "<<br/>"+ ratinga + "</html>");
                label2.setText("<html>" + captionb + "<br/>" + dateb + "<<br/>"+ ratingb + "</html>");
                label3.setText("<html>" + captionc + "<br/>" + datec + "<<br/>"+ ratingc + "</html>");
                label4.setText("<html>" + captiond + "<br/>" + dated + "<<br/>"+ ratingd + "</html>");
                label5.setText("<html>" + captione + "<br/>" + datee + "<<br/>"+ ratinge + "</html>");
            }

            if(e.getActionCommand().equals("Rating")) {//if you click on Rating
                Collections.sort(imageAlbum.photos, new CompareByRating());//uses comparebyrating method to sort photos 

                pics.clear();//clears ArrayList so that it is empty
                morepics.clear();                

                for (Photograph p : imageAlbum.photos) { //iterates through each photo in the imageAlbum 
                    if(p.getFilename().equals("Image5.jpg")) {//adds respective images to lists by matching the file name of the photo to the name of the photo
                        pics.add(icon1);
                        morepics.add(icon1a);    
                    }
                    if(p.getFilename().equals("Image4.jpg")) {
                        pics.add(icon2);
                        morepics.add(icon2b);
                    }
                    if(p.getFilename().equals("Image3.jpg")) {
                        pics.add(icon3);
                        morepics.add(icon3c);
                    }
                    if(p.getFilename().equals("Image1.jpg")) {
                        pics.add(icon4);
                        morepics.add(icon4d);
                    }
                    if(p.getFilename().equals("Image2.jpg")) {
                        pics.add(icon5);
                        morepics.add(icon5e);
                    }

                }                
                captiona = imageAlbum.photos.get(0).getCaption();//index of 0 gives caption of first image
                captionb = imageAlbum.photos.get(1).getCaption();//index of 1 gives caption of second image
                captionc = imageAlbum.photos.get(2).getCaption();
                captiond = imageAlbum.photos.get(3).getCaption();
                captione = imageAlbum.photos.get(4).getCaption();


                datea = imageAlbum.photos.get(0).getDateTaken();//index of 0 gives date of first image
                dateb = imageAlbum.photos.get(1).getDateTaken();
                datec = imageAlbum.photos.get(2).getDateTaken();
                dated = imageAlbum.photos.get(3).getDateTaken();
                datee = imageAlbum.photos.get(4).getDateTaken();

                ratinga = imageAlbum.photos.get(0).getRating() + "";//index of 0 gives rating of first image
                ratingb = imageAlbum.photos.get(1).getRating() + "";
                ratingc = imageAlbum.photos.get(2).getRating() + "";
                ratingd = imageAlbum.photos.get(3).getRating() + "";
                ratinge = imageAlbum.photos.get(4).getRating() + "";

                mainpic.setIcon(morepics.get(0));//resets the main pic
                label.setText("<html>" + captiona + "<br/>" + datea + "<<br/>"+ ratinga + "</html>");
                label2.setText("<html>" + captionb + "<br/>" + dateb + "<<br/>"+ ratingb + "</html>");
                label3.setText("<html>" + captionc + "<br/>" + datec + "<<br/>"+ ratingc + "</html>");
                label4.setText("<html>" + captiond + "<br/>" + dated + "<<br/>"+ ratingd + "</html>");
                label5.setText("<html>" + captione + "<br/>" + datee + "<<br/>"+ ratinge + "</html>");
                label.setIcon(pics.get(0));
                label2.setIcon(pics.get(1));
                label3.setIcon(pics.get(2));
                label4.setIcon(pics.get(3));
                label5.setIcon(pics.get(4));

                rating1 = morepics.indexOf(mainpic.getIcon());
                rating1 = imageAlbum.photos.get(rating1).getRating();
                if(rating1 == 1) { //if rating is one, then the button 1 will be selected 
                    one.setSelected(true);
                }if(rating1 == 2) {
                    two.setSelected(true);
                }if(rating1 == 3) {
                    three.setSelected(true);
                }if(rating1 == 4) {
                    four.setSelected(true);
                }if(rating1 == 5) {
                    five.setSelected(true);
                }

            }  

            if(e.getActionCommand().equals("Date")) { //if you click on Date
                Collections.sort(imageAlbum.photos);

                pics.clear();
                morepics.clear();                

                for (Photograph p : imageAlbum.photos) {
                    if(p.getFilename().equals("Image5.jpg")) {
                        pics.add(icon1);
                        morepics.add(icon1a);    
                    }
                    if(p.getFilename().equals("Image4.jpg")) {
                        pics.add(icon2);
                        morepics.add(icon2b);
                    }
                    if(p.getFilename().equals("Image3.jpg")) {
                        pics.add(icon3);
                        morepics.add(icon3c);
                    }
                    if(p.getFilename().equals("Image1.jpg")) {
                        pics.add(icon4);
                        morepics.add(icon4d);
                    }
                    if(p.getFilename().equals("Image2.jpg")) {
                        pics.add(icon5);
                        morepics.add(icon5e);
                    }

                }                
                captiona = imageAlbum.photos.get(0).getCaption();//index of 0 gives caption of first image
                captionb = imageAlbum.photos.get(1).getCaption();//index of 1 gives caption of second image
                captionc = imageAlbum.photos.get(2).getCaption();
                captiond = imageAlbum.photos.get(3).getCaption();
                captione = imageAlbum.photos.get(4).getCaption();


                datea = imageAlbum.photos.get(0).getDateTaken();//index of 0 gives date of first image
                dateb = imageAlbum.photos.get(1).getDateTaken();
                datec = imageAlbum.photos.get(2).getDateTaken();
                dated = imageAlbum.photos.get(3).getDateTaken();
                datee = imageAlbum.photos.get(4).getDateTaken();

                ratinga = imageAlbum.photos.get(0).getRating() + "";//index of 0 gives rating of first image
                ratingb = imageAlbum.photos.get(1).getRating() + "";
                ratingc = imageAlbum.photos.get(2).getRating() + "";
                ratingd = imageAlbum.photos.get(3).getRating() + "";
                ratinge = imageAlbum.photos.get(4).getRating() + "";

                mainpic.setIcon(morepics.get(0));
                label.setText("<html>" + captiona + "<br/>" + datea + "<<br/>"+ ratinga + "</html>");
                label2.setText("<html>" + captionb + "<br/>" + dateb + "<<br/>"+ ratingb + "</html>");
                label3.setText("<html>" + captionc + "<br/>" + datec + "<<br/>"+ ratingc + "</html>");
                label4.setText("<html>" + captiond + "<br/>" + dated + "<<br/>"+ ratingd + "</html>");
                label5.setText("<html>" + captione + "<br/>" + datee + "<<br/>"+ ratinge + "</html>");
                label.setIcon(pics.get(0));
                label2.setIcon(pics.get(1));
                label3.setIcon(pics.get(2));
                label4.setIcon(pics.get(3));
                label5.setIcon(pics.get(4));

                rating1 = morepics.indexOf(mainpic.getIcon());
                rating1 = imageAlbum.photos.get(rating1).getRating();
                if(rating1 == 1) {
                    one.setSelected(true);
                }if(rating1 == 2) {
                    two.setSelected(true);
                }if(rating1 == 3) {
                    three.setSelected(true);
                }if(rating1 == 4) {
                    four.setSelected(true);
                }if(rating1 == 5) {
                    five.setSelected(true);
                }

            }  
            if(e.getActionCommand().equals("Caption")) { //if you click on Caption
                Collections.sort(imageAlbum.photos, new CompareByCaption());//use comparebycaption method (lexographically) to sort the photos

                pics.clear();
                morepics.clear();                

                for (Photograph p : imageAlbum.photos) {
                    if(p.getFilename().equals("Image5.jpg")) {
                        pics.add(icon1);
                        morepics.add(icon1a);    
                    }
                    if(p.getFilename().equals("Image4.jpg")) {
                        pics.add(icon2);
                        morepics.add(icon2b);
                    }
                    if(p.getFilename().equals("Image3.jpg")) {
                        pics.add(icon3);
                        morepics.add(icon3c);
                    }
                    if(p.getFilename().equals("Image1.jpg")) {
                        pics.add(icon4);
                        morepics.add(icon4d);
                    }
                    if(p.getFilename().equals("Image2.jpg")) {
                        pics.add(icon5);
                        morepics.add(icon5e);
                    }

                }                
                captiona = imageAlbum.photos.get(0).getCaption();//index of 0 gives caption of first image
                captionb = imageAlbum.photos.get(1).getCaption();//index of 1 gives caption of second image
                captionc = imageAlbum.photos.get(2).getCaption();
                captiond = imageAlbum.photos.get(3).getCaption();
                captione = imageAlbum.photos.get(4).getCaption();


                datea = imageAlbum.photos.get(0).getDateTaken();//index of 0 gives date of first image
                dateb = imageAlbum.photos.get(1).getDateTaken();
                datec = imageAlbum.photos.get(2).getDateTaken();
                dated = imageAlbum.photos.get(3).getDateTaken();
                datee = imageAlbum.photos.get(4).getDateTaken();

                ratinga = imageAlbum.photos.get(0).getRating() + "";//index of 0 gives rating of first image
                ratingb = imageAlbum.photos.get(1).getRating() + "";
                ratingc = imageAlbum.photos.get(2).getRating() + "";
                ratingd = imageAlbum.photos.get(3).getRating() + "";
                ratinge = imageAlbum.photos.get(4).getRating() + "";

                mainpic.setIcon(morepics.get(0));
                label.setText("<html>" + captiona + "<br/>" + datea + "<<br/>"+ ratinga + "</html>");
                label2.setText("<html>" + captionb + "<br/>" + dateb + "<<br/>"+ ratingb + "</html>");
                label3.setText("<html>" + captionc + "<br/>" + datec + "<<br/>"+ ratingc + "</html>");
                label4.setText("<html>" + captiond + "<br/>" + dated + "<<br/>"+ ratingd + "</html>");
                label5.setText("<html>" + captione + "<br/>" + datee + "<<br/>"+ ratinge + "</html>");
                label.setIcon(pics.get(0));
                label2.setIcon(pics.get(1));
                label3.setIcon(pics.get(2));
                label4.setIcon(pics.get(3));
                label5.setIcon(pics.get(4));

                rating1 = morepics.indexOf(mainpic.getIcon());
                rating1 = imageAlbum.photos.get(rating1).getRating();
                if(rating1 == 1) {
                    one.setSelected(true);
                }if(rating1 == 2) {
                    two.setSelected(true);
                }if(rating1 == 3) {
                    three.setSelected(true);
                }if(rating1 == 4) {
                    four.setSelected(true);
                }if(rating1 == 5) {
                    five.setSelected(true);
                }

            }  

        }   
    }

    private class Click implements MouseListener{
        /**
         * This class called clicked is used when you use your mouse to do certain actions (click, release, etc...). The 
         * implementation of mouselistener interface is for receiving mouse events on a component.  
         * 
         */ 
        //source: lab powerpoint 
        //docs.oracle.com/javase/tutorial/uiswing/events/mouselistener.html.
        @Override
        public void mouseClicked(MouseEvent e) {// when you click the mouse 
            if(((JLabel)e.getSource()).getIcon().equals(icon1)) { //this sets larger image as the main/center pic when I click the thumbnail image  
                mainpic.setIcon(icon1a);
            }
            if(((JLabel)e.getSource()).getIcon().equals(icon2)) {
                mainpic.setIcon(icon2b);
            }
            if(((JLabel)e.getSource()).getIcon().equals(icon3)) {
                mainpic.setIcon(icon3c);
            }
            if(((JLabel)e.getSource()).getIcon().equals(icon4)) {
                mainpic.setIcon(icon4d);
            }
            if(((JLabel)e.getSource()).getIcon().equals(icon5)) {
                mainpic.setIcon(icon5e);
            }
            //this changes the rating on demand when you click on the image 
            rating1 = morepics.indexOf(mainpic.getIcon());
            rating1 = imageAlbum.photos.get(rating1).getRating();
            if(rating1 == 1) {
                one.setSelected(true);
            }if(rating1 == 2) {
                two.setSelected(true);
            }if(rating1 == 3) {
                three.setSelected(true);
            }if(rating1 == 4) {
                four.setSelected(true);
            }if(rating1 == 5) {
                five.setSelected(true);
            }

        }

        @Override
        public void mousePressed(MouseEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public void mouseReleased(MouseEvent e) {


        }

        @Override
        public void mouseEntered(MouseEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public void mouseExited(MouseEvent e) {
            // TODO Auto-generated method stub

        }

    }

    private static void createAndShowGUI() {
        /**
         * This class displays the GUI by using frame dimentions, frame title, frame color after adding the components to pane  
         * 
         */ 
        //displays GUI after adding componenets to the pane
        PhotoViewer frame = new PhotoViewer();
        frame.setTitle("My PhotoLibrary");
        frame.setBounds(5000, 1000, 5000, 1000);//size of frame
        frame.setBackground(Color.BLUE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Set up the content pane.
        frame.addComponentsToPane(frame.getContentPane());
        //Display the window.
        frame.setVisible(true);  
    }

    public static void main(String[] args) {
        /**
         * Write a test that creates a PhotoContainer containing 5 images and loads my photographs  
         * 
         */ 
        PhotoViewer myViewer = new PhotoViewer();
        String imageDirectory = "";

        Photograph p1 =new Photograph("Image1.jpg", imageDirectory + "Graduation!", "2018-06-02", 5);
        Photograph p2 =new Photograph("Image2.jpg", imageDirectory + "Rotunda!", "2018-09-30", 4);
        Photograph p3 =new Photograph("Image3.jpg", imageDirectory + "Christmas!", "2017-12-01", 5);
        Photograph p4 =new Photograph("Image4.jpg", imageDirectory + "Rafting!", "2017-08-12", 2);
        Photograph p5 =new Photograph("Image5.jpg", imageDirectory + "Awards!", "2017-04-30", 3);

        myViewer.imageAlbum = new PhotoLibrary("Fun times", 389374);
        myViewer.imageAlbum.addPhoto(p1);
        myViewer.imageAlbum.addPhoto(p2);
        myViewer.imageAlbum.addPhoto(p3);
        myViewer.imageAlbum.addPhoto(p4);
        myViewer.imageAlbum.addPhoto(p5);

        Collections.sort(myViewer.imageAlbum.photos);
        javax.swing.SwingUtilities.invokeLater(new Runnable() { 
            public void run() {      
                createAndShowGUI(); //invokes the createAndShowGUI method using a runnable object

            }
        });

    }

}

