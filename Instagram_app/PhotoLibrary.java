/**
 * Pooja Kaji (pk4bq) 
 * Sources :Big Java book, Professor Hott's Slides on Collab, and TA and Professor Hott's
 * office Hours
 */

import java.util.ArrayList;
import java.util.HashSet;

public class PhotoLibrary extends PhotographContainer {
    /**
     * Holds the name, id, HashSet of albums, and an ArryList of Photographs of the PhotoLibrary
     */
    private int id;
    private HashSet<Album> albums = new HashSet<>();

    /**
     * Instances name, albums, photos, and id are created
     */
    public PhotoLibrary(String name, int id) {
        super(name);
        this.id = id;
        this.albums = new HashSet<Album>();
        this.photos = new ArrayList<Photograph>();
    }

    // Getters and Setters

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the albums
     */
    public HashSet<Album> getAlbums() {
        return albums;
    }

    // other methods

    /**
     * Creates a new Album with name albumName and adds it to the list of albums, only if an Album with that name does not
     * already exist. Returns true if the add was successful, false otherwise.
     * 
     * @param An album name as a string
     * @returns true or false depending if adding the album was successful
     */
    public boolean createAlbum(String albumName) {
        Album a = new Album(albumName);
        if (albums.add(a)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Removes the Album with name albumName if an Album with that name exists in the set of albums. Returns true if the
     * remove was successful, false otherwise.
     * 
     * @param An album name as a string
     * @returns true or false depending if removing the album was successful
     */
    public boolean removeAlbum(String albumName) {
        return albums.remove(new Album(albumName));
    }

    /**
     * Add the Photograph p to the Album in the set of albums that has name albumName if and only if it is in the
     * PhotoLibrary’s list of photos and it was not already in that album. Return true if the Photograph was added; return
     * false if it was not added.
     * 
     * @param Photograph p and an Album Name– String
     * @returns true or false depending if adding the photograph was successful
     */
    public boolean addPhotoToAlbum(Photograph p, String albumName) {
        if (photos.contains(p)) {
            for (Album item : albums) {
                if (item.getName().equals(albumName)) {
                    if (!item.hasPhoto(p)) { // means if album does not have the photo
                        item.addPhoto(p);
                        return true;
                    }
                }
            }
        }
        return false;

    }

    /**
     * Remove the Photograph p from the Album in the set of albums that has name albumName. Return true if the photo was
     * successfully removed. Otherwise return false.
     * 
     * @param Photograph p and an Album Name– String
     * @returns true or false depending if removing the photograph was successful
     */
    public boolean removePhotoFromAlbum(Photograph p, String albumName) {
        for (Album item : albums) {
            if (item.getName().equals(albumName)) { // checking if item object is equal to the album name which is also an object
                if (item.hasPhoto(p)) {
                    item.removePhoto(p);
                    return true;
                }
            }
        }
        return false;

    }

    /**
     * Given an album name, return the Album with that namefrom the set of albums. If an album with that name is not found,
     * return null.
     * 
     * @param Photograph p
     * @returns albumname or null if name is not found
     */
    private Album getAlbumByName(String albumName) {
        for (Album item : albums) {
            if (item.getName().equals(albumName)) {
                return item;
            }
        }
        return null;
    }

    /**
     * If Photograph p is in the current PhotoLibrary object’s list of Photographs, removes p from the current object’s list
     * and album.
     * 
     * @param Photograph p
     * @returns true if the Photograph was removed or false if it was not there.
     */

    @Override
    public boolean removePhoto(Photograph p) {
        if (photos.contains(p)) {
            photos.remove(p);
            for (Album item : albums) {
                item.removePhoto(p);
            }
            return true;
        } else {
            return false;

        }

    }

    /**
     * Given an object o, return true if the currentPhoto Library object’s id value is equal to the id value of the Photo
     * Library object passed to equals(). Otherwise, return false.
     * 
     * @param Object o
     * @return whether or not the Photo Library object’s id value is equal to the id value of the Photo Library object
     */

    public boolean equals(Object o) {
        if (o == null) { // o is null
            return false;
        }
        if (!(o instanceof PhotoLibrary)) {
            return false; // o is not a PhotoLibrary
        }
        PhotoLibrary oPhotoLibrary = (PhotoLibrary) o; // Re-cast o to a PhotoLibrary
        if (this.id == oPhotoLibrary.getId()) {
            return true;
        } else {
            return false; // o has wrong id
        }
    }

    /**
     * Generates a String that shows the values of the fields name, id, photos, and albums.
     * 
     * @return string
     */

    public String toString() {
        return "(Name: " + this.name + ", " + "ID: " + this.id + ", " + "Photos: " + this.photos + ", " + "Albums: " + this.albums
                + ")";
    }

    /**
     * This method compares the photographs of PhotoLibrary a with PhotoLibrary b and creates a new ArrayList with all the
     * common photographs
     * 
     * @param PhotoLibrary a and PhotoLibrary b
     * @return an ArrayList<Photograph> of the photos that both PhotoLibrary a and PhotoLibrary b have.
     */
    public static ArrayList<Photograph> commonPhotos(PhotoLibrary a, PhotoLibrary b) {
        ArrayList<Photograph> newphotos = new ArrayList<Photograph>();
        for (int i = 0; i < a.photos.size(); i++) {
            for (int s = 0; s < b.photos.size(); s++) { // here is a nested for loop to compare each element in b to every
                // element in a
                if (a.photos.get(i).equals(b.photos.get(s))) {
                    newphotos.add(a.photos.get(i));
                }
            }
        }
        return newphotos;
    }

    /**
     * This method measures how similar the photo feeds are between PhotoLibrary a and PhotoLibrary b, in between 0 and 1.
     * If either PhotoLibrary has no photos, it is 0.0. Otherwise, it is the number of commonPhotos to both PhotoLibraries
     * divided by smaller of the number of photos a has and the number of photos b has
     * 
     * @param PhotoLibrary a and PhotoLibrary b
     * @returns a double between 0 and 1
     */
    public static double similarity(PhotoLibrary a, PhotoLibrary b) {
        ArrayList<Photograph> newphotos = (commonPhotos(a, b));
        int anumber = a.photos.size();
        int bnumber = b.photos.size();
        if (anumber == 0 || (bnumber == 0)) {
            return 0.0;
        } else if (anumber < bnumber) {
            return (double) newphotos.size() / anumber;
        } else if (bnumber < anumber) {
            return (double) newphotos.size() / bnumber;
        } else {
            return (double) newphotos.size() / bnumber;
        }
    }

    // main method
    public static void main(String[] args) {
        // testing boolean takePhoto method(1)
        PhotoLibrary p1 = new PhotoLibrary("Nikita", 1217); // initializes a PhotoLibrary with name Nikita and id number 1217
        Photograph p2 = new Photograph("I am cold", "12345.jpg", "2018-07-12", 2);
        boolean photoa = p1.addPhoto(p2);// uses the method takePhoto with PhotoLibrary p1 and photograph p2
        System.out.println(photoa + "");

        // testing boolean takePhoto method(2)
        boolean photob = p1.addPhoto(p2);// uses the method takePhoto with PhotoLibrary p1 and photograph p2
        System.out.println(photob + "");

        // testing boolean takePhoto method(3)
        PhotoLibrary p3 = new PhotoLibrary("Pooja", 9928);// initializes a PhotoLibrary with name Pooja and id number 9928
        Photograph p4 = new Photograph("Cool Bro", "54321.jpg", "2018-01-10", 1);
        boolean photoc = p3.addPhoto(p4);// uses the method takePhoto with PhotoLibrary p3 and photograph p4
        System.out.println(photoc + "");

        // testing boolean hasPhoto method(1)
        boolean photod = p1.hasPhoto(p2);// uses the method hasPhoto with PhotoLibrary p1 and photograph p2
        System.out.println(photod + "");

        // testing boolean hasPhoto method(2)
        boolean photoe = p3.hasPhoto(p2);// uses the method hasPhoto with PhotoLibrary p3 and photograph p2
        System.out.println(photoe + "");

        // testing boolean erasePhoto method(1)
        boolean photof = p1.removePhoto(p4);// uses the method erasePhoto with PhotoLibrary p1 and photograph p4
        System.out.println(photof + "");

        // testing boolean erasePhoto method(2)
        boolean photog = p1.removePhoto(p2);// uses the method erasePhoto with PhotoLibrary p1 and photograph p2
        System.out.println(photog + "");

        // testing boolean erasePhoto method(3)
        // boolean photoh = p1.erasePhoto(p2);// uses the method erasehasPhoto with PhotoLibrary p1 and photograph p2
        // System.out.println(photoh + "");

        // testing public int numPhotographs method(1)
        boolean photoi = p3.addPhoto(p2);// uses the takePhoto method to add to the ArrayList
        int size = p3.numPhotographs();// finds size of ArrayList of object p3
        System.out.println(photoi + "");
        System.out.println(size + "");

        // testing public int numPhotographs method(2)
        // boolean photoj = p3.erasePhoto(p2);// uses the erasePhoto method to remove item from the ArrayList
        int length = p3.numPhotographs();// finds size of ArrayList of object p3
        // System.out.println(photoj + "");
        System.out.println(length + "");

        // testing toString method(1)
        System.out.println(p1);// prints out "Nikita" which is the name and 1217 which is the ID number

        // testing toString method(2)
        System.out.println(p3);// prints out "Pooja" which is the name and 9928 which is the ID number

        // testing boolean equals method(1)
        PhotoLibrary p5 = new PhotoLibrary("Karen", 814);// initalizes a new PhotoLibrary object with name Karen and id 814 and
        // comapres
        // her id to
        // Nikita's id
        boolean test = p1.equals(p5);
        System.out.println(test + "");

        // testing boolean equals method(2)
        PhotoLibrary p6 = new PhotoLibrary("Karen", 9928);// initalizes a new PhotoLibrary object with name Karen and id 9928 and
        // comapres her id to
        // Nikita's id
        boolean compare = p3.equals(p6);
        System.out.println(compare + "");

        // testing boolean equals method(3)
        PhotoLibrary p7 = null;// initalizes a new PhotoLibrary object as null and then comapres his/her id to Nikita's id
        boolean simple = p3.equals(p7);
        System.out.println(simple + "");

        // testing public static ArrayList<Photograph> method(1)
        boolean photok = p1.addPhoto(p2);// uses takePhoto method to add photograph p2 to PhotoLibrary p1
        boolean photol = p3.addPhoto(p2);// uses takePhoto method to add photograph p2 to PhotoLibrary p3
        System.out.println(commonPhotos(p1, p3));

        // testing public static ArrayList<Photograph> method(2)
        Photograph p8 = new Photograph("Ice Cream", "capString.jpg", "2018-11-12", 3);
        boolean photom = p1.addPhoto(p8);// uses takePhoto method to add photograph p8 to PhotoLibrary p1
        System.out.println(commonPhotos(p1, p3));

        // testing public static double similarity(PhotoLibrary a, PhotoLibrary b) method(1)
        System.out.println(similarity(p1, p3));

        // testing public static ArrayList<Photograph> method(3)
        Photograph p9 = new Photograph("I love CS", "capString.jpg", "2018-11-12", 3);
        Photograph p10 = new Photograph("I love math", "sunny.jpg", "2011-3-09", 2);
        Photograph p11 = new Photograph("Happy Birthday", "2519.jpg", "2018-09-12", 4);
        Photograph p12 = new Photograph("Sweet Dreams", "photofile.jpg", "2018-06-12", 5);
        boolean photon = p3.addPhoto(p8);// uses takePhoto method to add photograph p8 to PhotoLibrary p3
        boolean photoo = p3.addPhoto(p9);// uses takePhoto method to add photograph p9 to PhotoLibrary p3
        boolean photop = p3.addPhoto(p10);// uses takePhoto method to add photograph p10 to PhotoLibrary p3
        boolean photoq = p1.addPhoto(p11);// uses takePhoto method to add photograph p11 to PhotoLibrary p1
        System.out.println(commonPhotos(p1, p3));

        // testing public static double similarity(PhotoLibrary a, PhotoLibrary b) method(2)
        System.out.println(similarity(p1, p3));

        // testing public static double similarity(PhotoLibrary a, PhotoLibrary b) method(3)
        System.out.println(similarity(p1, p5));

    }
}
