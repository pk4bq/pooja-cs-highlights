import java.io.File; //assumption: must import file from Java.io

/**
 * Pooja Kaji (pk4bq) 
 * Sources :Big Java book, Professor Hott's Slides on Collab, and TA and Professor Hott's
 * office Hours
 */

public class Photograph implements Comparable<Photograph> {
    /**
     * Fields Holds the caption, filename, date taken, file image, and rating of a photograph
     */
    
    private String caption;
    private String filename;
    private String dateTaken;
    private File imageFile;
    private int rating;

    /**
     * Constructor1 Instances caption, filename, and file image
     */
    public Photograph(String caption, String filename) { 
        this.caption = caption;
        this.filename = filename;
        imageFile = new File(filename);; //assumption: file instatiated but not part of photograph object

    }


    /**
     * Constructor2 Instances caption, file image, filename, dateTaken, and rating are created
     */
    public Photograph(String filename, String caption, String dateTaken, int rating) {
        this.caption = caption;
        this.filename = filename;
        this.dateTaken = dateTaken;
        this.rating = rating;
        imageFile = new File(filename); //assumption: file initiated but not part of photograph object

    }
    
    /**
     * @return the imageFile
     */
    public File getImageFile() {
        return imageFile;
    }

    /**
     * @param imageFile the imageFile to set
     */
    public void setImageFile(File imageFile) {
        this.imageFile = imageFile;
    }

    /**
     * @return the caption
     */
    public String getCaption() {
        return caption;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @return the dateTaken
     */
    public String getDateTaken() {
        return dateTaken;
    }

    /**
     * @param dateTaken the dateTaken to set
     */
    public void setDateTaken(String dateTaken) {
        this.dateTaken = dateTaken;
    }

    /**
     * @return the rating
     */
    public int getRating() {
        return rating;
    }

    /**
     * @param rating the rating to set
     */
    public void setRating(int rating) {
        if (rating >= 0 && rating <= 5) { // rating as to be between 0 and 5 so this is an exception
            this.rating = rating;
        }
    }

    @Override
    public int hashCode() {
        return (this.caption + "---" + this.filename).hashCode();
    }

    /**
     * Boolean equals methods tests if object o is equal to the filename and caption. Returns true if the Photograph object
     * passed to equals() with caption and filename strings match the caption and filename strings of the current Photograph
     * object; otherwise, return false.
     * 
     * @param Object o
     * @retrun whether the caption and filename of two Photograph objects are equal to each other
     */
    public boolean equals(Object o) {
        if (o == null) {// o is null
            return false;
        }
        if (!(o instanceof Photograph)) {
            return false; // o is not a Photograph
        }
        Photograph oPhotograph = (Photograph) o; // Re-cast o to a Photograph
        if (this.caption.equals(oPhotograph.caption) && (this.filename.equals(oPhotograph.filename))) {
            return true;
        } else {
            return false; // o has wrong filename or caption
        }
    }

    /**
     * Compares the dateTaken of the current Photograph object with the parameter p. If the current object’s dateTaken is
     * before p’s, return a negative number. If p’s is earlier, return a positive number. If they are equal, return the
     * comparison of the this object’s caption with p’s caption.
     * 
     * @param Photograph p
     * @retrun a postive or negative integer
     */
    public int compareTo(Photograph p) {
        if (this.dateTaken.compareTo(p.getDateTaken()) == 0) {
            if (this.getCaption().compareTo(p.getCaption()) > 0) {
                return 1;
            }
            if (this.getCaption().compareTo(p.getCaption()) < 0) {
                return -1;
            } else {
                return 0;
            }
        }
        if (this.dateTaken.compareTo(p.getDateTaken()) > 0) {
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * toString method generates a string that shows the values of the fields caption, filename, date taken, and rating.
     * 
     * @retrun string representation
     */
    public String toString() {
        return "(Caption: " + this.caption + ", " + "Filename: " + this.filename + ", " + "Date Taken: " + this.dateTaken + ","
                + "Rating: " + this.rating + ")";
    }

    // main method
    public static void main(String[] args) {

        // testing toString method(1)
        Photograph p1 = new Photograph("Hello", "Pooja's Photo.jpg", "2016-12-09", 5);// creates new Photogrpah object p1
        System.out.println(p1);
        // testing toString method(2)
        Photograph p2 = new Photograph("1234", "XYXNANOBOT.jpg", "2017-10-01", 3);// creates new Photogrpah object p2
        System.out.println(p2);

        // testing boolean equals method(1)
        boolean combo = p1.equals(p2);
        System.out.println(combo + "");

        // testing boolean equals method(2)
        Photograph p3 = new Photograph("Hello", "Pooja's Photo.jpg", "2013-3-02", 5);// creates new Photogrpah object p3
        boolean cool = p1.equals(p3);
        System.out.println(cool + "");

        // testing boolean equals method(3)
        Photograph p4 = null; // sets a new photograph object eqauls to null and then comapres its caption and filename to object
        // p3
        boolean simple = p3.equals(p4);
        System.out.println(simple + "");
    }
}
